//
//  ProfileInfo.m
//  
//
//  Created by Abbas H Safaie on 27/07/2015.
//
//

#import "ProfileInfo.h"


@implementation ProfileInfo

@dynamic fullName;
@dynamic profileImage;

@end
