//
//  AccessToken.m
//  GriffithThreaserTry3
//
//  Created by Abbas H Safaie on 7/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import "AccessToken.h"


@implementation AccessToken

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.lockBox=[[Lockbox alloc]init];
    }
    return self;
}

/*Methhod to set accessToken in KeyChain */
-(BOOL) AccessTokenSet{
    
    NSString * checker=[self.lockBox stringForKey:ACCESS_KEY_TOKEN];
    
    if(self.accessTokenOb != nil && checker == nil){  //check for existing and nil passed object 
        
        BOOL boolReturn=[self.lockBox setString:self.accessTokenOb forKey:ACCESS_KEY_TOKEN];
    
        return boolReturn;
    
    }else{
        if(checker!=nil)
            NSLog(@"Key exist ");
        else
        NSLog(@"AccessToken String was Empty");
        return false;
    }
}

/*Mthod Return access Token Key as a String*/
-(NSString*) AccessTokenGet{

    
    NSString * retString=[self.lockBox stringForKey:ACCESS_KEY_TOKEN];
    
    if(retString!=nil)
    return retString;
    else
        NSLog(@"AccessToken Not Exist");
    return nil;
}

//get Header for Api call with AccessToken 
-(NSDictionary*) AccessTokenHeader{

    
    NSString * retString=[self.lockBox stringForKey:ACCESS_KEY_TOKEN];
    
    if(retString!=nil){
      
        NSDictionary * header=@{AZURE_HEADER_Token_NAME:retString};
    
    return header;
        
    }else{
        NSLog(@"AccessToken Not Exist");
        return nil;
    }

}


//Check if registerd loged in before or not
-(BOOL) AccessTokenChecker{

        NSString * checker=[self.lockBox stringForKey:ACCESS_KEY_TOKEN];
    NSLog(@"%@",checker);
        return  (checker!=nil)? true:false;
}



@end
