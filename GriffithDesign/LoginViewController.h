//
//  LoginViewController.h
//  GriffithDesign
//
//  Created by Abbas H Safaie on 22/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@end
