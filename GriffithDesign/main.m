//
//  main.m
//  GriffithDesign
//
//  Created by Abbas H Safaie on 21/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
