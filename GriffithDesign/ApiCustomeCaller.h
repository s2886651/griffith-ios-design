//
//  ApiCustomeCaller.h
//  GriffithThreaserTry3
//
//  Created by Abbas H Safaie on 7/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WindowsAzureMobileServices/WindowsAzureMobileServices.h>
#define APP_URL_AZURE @"https://griffithtreaserfinder.azure-mobile.net/"
#define APP_KEY @"cdDskiShyjPQQjjTJKRdIINfUQIXwi32"
#define LOGIN_RESPONSECODE  @"loginResponseCode"  //return type respondObject
#define POSTS_UPDATE @"UpdatePosts"
@interface ApiCustomeCaller : NSObject

@property (nonatomic,strong) MSClient * client;

//Azure InvokeApi method Properties
@property NSString * apiName;
@property NSString * method;
@property id body;
@property NSDictionary * header;
@property NSDictionary * parameters;
@property (nonatomic,strong) NSMutableDictionary* dic;


-(void) apiCallHandler:(NSString*)notificationName;; //Method to handle POST Mesages Or GET without return
-(void) loginHandler ; //Method : handler for login process

-(void) apiCallWithAuthheader;
@end
