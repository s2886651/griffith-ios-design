//
//  ProfileViewController.m
//  GriffithDesign
//
//  Created by Abbas H Safaie on 22/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import "ProfileViewController.h"
#import "ProfileDto.h"
#import "ApiCustomeCaller.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "Profile.h"



@interface ProfileViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuButton;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UIImageView *headerImage;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *nameTextField;
@property (weak, nonatomic) IBOutlet UILabel *campusTextField;
@property (weak, nonatomic) IBOutlet UILabel *sexTextField;
@property (weak, nonatomic) IBOutlet UILabel *majorTextField;
@property (weak, nonatomic) IBOutlet UILabel *entryTextField;
@end

@implementation ProfileViewController


/*NsManageObjectContec from delegate
 */
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


//Check if profile Exist 
-(BOOL) checkerForProfile{

    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Profile"];
    NSArray * array=[[managedObjectContext executeFetchRequest:fetchRequest error:nil]mutableCopy];
    if (array.count>0) {
        NSLog(@"Profile exist");
        return YES;
    }
    return NO;
}


//Check if profile Exist
-(BOOL) checkForPRofileImage{
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Profile"];
    NSArray * array=[[managedObjectContext executeFetchRequest:fetchRequest error:nil]mutableCopy];
    if (array.count>0) {
        
        NSManagedObject * profile=[array objectAtIndex:0];
        if ([profile valueForKey:@"image"]==nil) {
            return NO;
        }
}
        return YES;
}



- (void)viewDidLoad {
    [super viewDidLoad];

    
    //menu
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.menuButton setTarget: self.revealViewController];
        [self.menuButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    //profile image layout
    self.profileImage.layer.backgroundColor=
    [[UIColor clearColor] CGColor];
    self.profileImage.layer.cornerRadius=20;
    self.profileImage.layer.borderWidth=2.0;
    self.profileImage.layer.masksToBounds = YES;
    self.profileImage.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    
    
    //IF data exist in Core data ,Load
    if ([self checkerForProfile]) {
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Profile"];
        NSError * error=nil;
        NSArray * array=[[managedObjectContext executeFetchRequest:fetchRequest error:&error]mutableCopy];
        Profile * profile=(Profile*)[array objectAtIndex:0];


        NSData *imageData =profile.image;
        NSString * firstName=profile.firstName;
        NSString * lastName=profile.lastName;
        NSString * fullName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
        self.majorTextField.text=profile.major;
        self.campusTextField.text=profile.campus;
        
        if([profile.sex isEqual: @"M"])
            self.sexTextField.text=@"He";
        else
            self.sexTextField.text=@"She"
            ;

        self.nameTextField.text=fullName;
        self.profileImage.image = [UIImage imageWithData:imageData];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy"];
        NSDate * date= [profile valueForKey:@"entryYear"];
        self.entryTextField.text=[formatter stringFromDate:date];
    }else{ //if data not exist in core data call api
    //api call
    ApiCustomeCaller * api=[[ApiCustomeCaller alloc]init];
    api.method=@"GET";
    api.apiName=@"UserProfile";
    [api apiCallHandler:@"userProfile"];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(recivednotifica:) name:@"userProfile" object:nil];
    
    }
    if (![self checkForPRofileImage]) {  //check if image exist in coreData if not call api
        
    
    ApiCustomeCaller * api2=[[ApiCustomeCaller alloc]init];
    api2.method=@"GET";
    api2.apiName=@"Images";
    [api2 apiCallHandler:@"userIMages"];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(recivedImages:) name:@"userIMages" object:nil];
    }
    

    
    
}

/*
 * notification data return from API
 */
-(void)recivedImages:(NSNotification *)notification{
    
    NSArray * images=(NSArray*)[notification userInfo];
    NSURL *imageURL = [NSURL URLWithString:[images objectAtIndex:3]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        if (![self checkForPRofileImage]) {
                NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Profile"];
                NSError * error=nil;
                NSArray * array=[[managedObjectContext executeFetchRequest:fetchRequest error:&error]mutableCopy];
                 NSManagedObject * profile=[array objectAtIndex:0];
            [profile setValue:imageData forKey:@"image"];
            
            if (![managedObjectContext save:&error]) {
                NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
            }

        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            self.profileImage.image = [UIImage imageWithData:imageData];
            
        });
    });
    
    
}

/*
 Call back data from AP call PROFILE
 */
-(void)recivednotifica:(NSNotification *)notification {
    NSDictionary* userI= [notification userInfo];
    NSLog(@"Notoiifcatio recived : %@",[userI valueForKey:@"firstName"]);
    
    
    NSString * first=[userI valueForKey:Profile_firstName];
    NSString * middle=[userI valueForKey:Profile_middleName];
    NSString * last=[userI valueForKey:Profile_lastName];
    NSString * major=[userI valueForKey:Profile_major];
    NSString * sex=[userI valueForKey:Profile_sex];
    NSDate * entryYear=[userI valueForKey:Profile_entryYear];
    NSString * fullName=[NSString stringWithFormat:@"%@ %@",first,last];
    
  
#pragma Mark - core data
    //Save to core data
    //Check if user dosent exist save it
    if (![self checkerForProfile]) {
    NSManagedObjectContext *context = [self managedObjectContext];
    // Create a new managed object
    NSManagedObject *userProfile = [NSEntityDescription insertNewObjectForEntityForName:@"Profile" inManagedObjectContext:context];
        [userProfile setValue:first forKey:@"firstName"];
        [userProfile setValue:middle forKey:@"middleName"];
        [userProfile setValue:last forKey:@"lastName"];
        [userProfile setValue:major forKey:@"major"];
        [userProfile setValue:sex forKey:@"sex"];
        [userProfile setValue:entryYear forKey:@"entryYear"];
    //[userProfile setValue:nil forKey:@"image"];
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
      }
    }
    
    
    
    self.nameTextField.text=fullName;

    if([[userI valueForKey:Profile_sex] isEqual: @"M"])
        self.sexTextField.text=@"He";
    else
        self.sexTextField.text=@"She"
        ;
    
    self.majorTextField.text=[userI objectForKey:Profile_major];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSDate * date= [userI objectForKey:Profile_entryYear];;
    self.entryTextField.text=[formatter stringFromDate:date];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
