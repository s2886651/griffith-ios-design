//
//  ImageDto.m
//  GriffithThreaserTry3
//
//  Created by Abbas H Safaie on 10/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import "ImageDto.h"

@implementation ImageDto



- (instancetype)init
{
    self = [super init];
    if (self) {
        self.client=[MSClient clientWithApplicationURLString:APP_URL_AZURE
                                                                applicationKey:APP_KEY];
    }
    return self;
}


/* save image into the BlockBlob */


-(void) saveImageToBlob:(NSData*)image {
   

    
    AccessToken * acc=[[AccessToken alloc]init];
    
    NSString * returnKey=[acc AccessTokenGet];
    
    NSString * headerpart1=AZURE_HEADER_Token_NAME;
    NSDictionary * header=@{headerpart1:returnKey};
    [self.client invokeAPI:@"Images"
                 body:nil
           HTTPMethod:@"POST"
           parameters:nil
              headers:header
           completion:^(id result,
                        NSHTTPURLResponse *response,
                        NSError *error) {
               //do whatever you need to do for result so
               NSLog(@"%@",response);
               NSDictionary * imageInfo=result;
               NSString * sasUrl=[imageInfo valueForKey:@"sas"];
               NSString * dateUtom=[imageInfo valueForKey:@"utcDate"];
               //send the image to blob
               [self storeImageToBlob:image :sasUrl :dateUtom];
           }];

}

//Upload image to azure blob container with Sas url

-(void) storeImageToBlob:(NSData*)imageData :(NSString*)sasUrl :(NSString*)dateUto{
    
  
    if (imageData.length!=0) {
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:sasUrl]];
        [request setHTTPMethod:@"PUT"];
        [request addValue:@"image/jpeg" forHTTPHeaderField:@"Content-Type"];
        //header needed for Azure blob
        [request addValue:dateUto forHTTPHeaderField:@"x-ms-date"];
        [request addValue:@"BlockBlob" forHTTPHeaderField:@"x-ms-blob-type"];
        [request setHTTPBody:imageData];
        self.conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        _recivedData = [[NSMutableData alloc] init];
        [_recivedData setLength:0];
    }
}


#pragma Prtocols for NsURLconnection


-(void)connection:(NSConnection*)conn didReceiveResponse:
(NSURLResponse *)response
{
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    if ([httpResponse statusCode] >= 400) {
        NSLog(@"Status Code: %li", (long)[httpResponse statusCode]);
        NSLog(@"Remote url returned error %ld %@",(long)[httpResponse statusCode],[NSHTTPURLResponse localizedStringForStatusCode:[httpResponse statusCode]]);
    }
    else {
        NSLog(@"Safe Response Code: %li", (long)[httpResponse statusCode]);
    }
}




- (void)connection:(NSURLConnection *)connection didReceiveData:
(NSData *)data
{
    [_recivedData appendData:data];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:
(NSError *)error
{
    //We should do something more with the error handling here
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:
           NSURLErrorFailingURLStringErrorKey]);
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
   
}





@end
