//
//  AccessToken.h
//  GriffithThreaserTry3
//
//  Created by Abbas H Safaie on 7/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Lockbox.h"
#define ACCESS_KEY_TOKEN @"tokenKey"        //KeyName that getting saved for accessToken value in vult
#define AZURE_HEADER_Token_NAME @"X-ZUMO-AUTH"      //Header Need to get Defien as Key for value Token
#define AccessTokenKey   @"mobileServiceAuthenticationToken"   //define key that return with AuthToken from API


@interface AccessToken : NSObject

@property NSString * accessTokenOb;

@property Lockbox * lockBox;


-(BOOL) AccessTokenSet;//Methhod to set accessToken in KeyChain 

-(NSString*) AccessTokenGet;//Mthod Return access Token Key as a String

-(NSDictionary*) AccessTokenHeader;

-(BOOL) AccessTokenChecker;
@end
