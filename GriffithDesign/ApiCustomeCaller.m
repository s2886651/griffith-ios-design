//
//  ApiCustomeCaller.m
//  GriffithThreaserTry3
//
//  Created by Abbas H Safaie on 7/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//
#import <WindowsAzureMobileServices/WindowsAzureMobileServices.h>
#import "ApiCustomeCaller.h"
#import "AccessToken.h"



@implementation ApiCustomeCaller

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        
        self.client=[MSClient clientWithApplicationURLString:APP_URL_AZURE
                                                      applicationKey:APP_KEY];

      //  self.dic=[[NSMutableDictionary alloc]init];
    }
    return self;
}




//Post handler   for table with notification Center

-(void) apiCallHandler :(NSString*)notificationName{

    AccessToken * acc=[[AccessToken alloc]init];
    NSDictionary * head=[acc AccessTokenHeader];
 //Calling Api to get the Json Data
         [self.client invokeAPI:self.apiName
               body:self.body
         HTTPMethod:self.method
         parameters:nil
            headers:head
         completion:^(id result,
                      NSHTTPURLResponse *response,
                      NSError *error) {
         //do whatever you need to do for result so
             
             NSLog(@"%@",response);
             
             if ([notificationName isEqual:POSTS_UPDATE]) {
                 
                 [self resultNotified:response :POSTS_UPDATE];
                 
             }else{
             [self resultNotified:result :notificationName];
                    }
             
         }];
}



-(void) apiCallWithAuthheader{
    AccessToken * acc=[[AccessToken alloc]init];
    NSDictionary * head=[acc AccessTokenHeader];
    //Calling Api to get the Json Data
    [self.client invokeAPI:self.apiName
                      body:self.body
                HTTPMethod:self.method
                parameters:nil
                   headers:head
                completion:^(id result,
                             NSHTTPURLResponse *response,
                             NSError *error) {
                    //do whatever you need to do for result so
                    
                    NSLog(@"%@",response);
                }];
}



/*
 * apiCallResult 
 *Send notification that result recived
 */
-(void) resultNotified:(id)objectPass :(NSString*)notificationName {
   
    NSNotificationCenter * noti=[NSNotificationCenter defaultCenter];
    
            [noti postNotificationName:notificationName object:self userInfo:objectPass];
    
 }





-(void) loginHandler{
    
    MSClient * myClient=[MSClient clientWithApplicationURLString:APP_URL_AZURE
                                                  applicationKey:APP_KEY];
    //Calling Api to get the Json Data
    
    [myClient invokeAPI:@"CustomLogin"
                   body:self.body
             HTTPMethod:@"POST"
             parameters:nil
                headers:nil
             completion:^(id result,
                          NSHTTPURLResponse *response,
                          NSError *error) {
                 
                 NSHTTPURLResponse * respondObject=(NSHTTPURLResponse*)response;
                 long status=[respondObject statusCode];
                 
                 //Created
                 if (status==200) {
                     AccessToken * accessClass=[[AccessToken alloc]init];
                     accessClass.accessTokenOb=[result valueForKey:AccessTokenKey];
                     [accessClass AccessTokenSet];
                      [self resultNotified:respondObject :LOGIN_RESPONSECODE ];//send respond
                 }
                 else{ //Problem
                     [self resultNotified:respondObject :LOGIN_RESPONSECODE ];//send respond
                     
                 }
             }];
}

 



@end
