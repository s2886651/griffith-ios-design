//
//  Profile.m
//  
//
//  Created by Abbas H Safaie on 27/07/2015.
//
//

#import "Profile.h"


@implementation Profile

@dynamic firstName;
@dynamic middleName;
@dynamic lastName;
@dynamic major;
@dynamic sex;
@dynamic campus;
@dynamic entryYear;
@dynamic birthDay;
@dynamic image;

@end
