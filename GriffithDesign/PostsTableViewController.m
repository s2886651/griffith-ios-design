
//  PostsTableViewController.m
//  GriffithThreaserTry3
//
//  Created by Abbas H Safaie on 15/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import "PostsTableViewController.h"
#import "ApiCustomeCaller.h"
#import "NewsFeedCellTableViewCell.h"
#import "SWRevealViewController.h"
#import "PostCell.h"


@interface PostsTableViewController ()
@property NSArray * returnDic;
@property NSString * stringD;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuButton;

@end

@implementation PostsTableViewController


//Capture and refresh Table
-(void)recivednotifica:(NSNotification *)notification {
    self.returnDic=(NSArray*)[notification userInfo];
    NSLog(@"%@",[self.returnDic objectAtIndex:0]);
    [self.tableView reloadData];
}

-(void) getPosts{

    ApiCustomeCaller * apiCall=[[ApiCustomeCaller alloc]init];
    apiCall.apiName=@"Posts";
    apiCall.method=@"GET";
    [apiCall apiCallHandler:@"apiCallResult"];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(recivednotifica:) name:@"apiCallResult" object:nil];
}

//update table after post sended

-(void)updatePosts:(NSNotification *)notification{

    [self getPosts];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    //SlideMenu
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.menuButton setTarget: self.revealViewController];
        [self.menuButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updatePosts:) name:POSTS_UPDATE object:nil];    //Api call for Post
    [self getPosts];
    
 
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {


    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    //long nu=self.returnDic.count;
    return self.returnDic.count+1;
}

//row heights for different cells

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 87;
    }
    else {
        return 200;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    
    
    if (indexPath.row==0) {
        
        PostCell * cell=[tableView dequeueReusableCellWithIdentifier:@"Post"];
        
        if (cell==nil) {
            cell=[[PostCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Post"];
            
        }
        
        cell.postText.text=@"";
        
        return cell;
    
    }else{
    
        NewsFeedCellTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"Feed"];
        if (cell==nil) {
            cell=[[NewsFeedCellTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Feed"];
        }
        //Post dictionary
        NSDictionary * post = [self.returnDic objectAtIndex:indexPath.row-1];
          //cooments attached to post

            cell.feedName.text=[post objectForKey:@"title"];
        
            cell.feedTextField.text=[post objectForKey:@"description"];
        NSDate* timeStamp=[post objectForKey:@"createdAt"];
        
        NSString *dateString = [NSDateFormatter localizedStringFromDate:timeStamp
                                                              dateStyle:NSDateFormatterShortStyle
                                                              timeStyle:NSDateFormatterShortStyle];
        cell.timeStamp.text=dateString;
        
        return cell;
    }

    
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"commentPush" sender:self];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
  //  NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
   // NSDictionary * post = [self.returnDic objectAtIndex:indexPath.row];
 
//    if ([[segue identifier] isEqualToString:@"commentPush"]) {
//        CommentsViewController * cvc=[segue destinationViewController];
//        cvc.comments=[post objectForKey:@"comments"];
//        cvc.postdescription=
//       [post objectForKey:@"description"];
//        //Set Key for post 
//        cvc.postId=[post objectForKey:@"id"];
//        
//    }
    
    
    
}


@end
