//
//  SlideMenuTableViewController.m
//  GriffithDesign
//
//  Created by Abbas H Safaie on 21/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import "SlideMenuTableViewController.h"
#import "AppDelegate.h"
#import "ProfileMenuCell.h"
#import <QuartzCore/QuartzCore.h>


@interface SlideMenuTableViewController ()
@property NSArray * menuItems;
@end

@implementation SlideMenuTableViewController

/*Context*/
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

-(NSManagedObject*) getProfileName{


    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"ProfileInfo"];
    NSArray * profiles=[[managedObjectContext executeFetchRequest:fetchRequest error:nil]mutableCopy];
    NSManagedObject* profile=[profiles objectAtIndex:0];
   

    return profile;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    

    self.menuItems=@[@"Profile",@"NewsFeed"];
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Set the title of navigation bar by using the menu items
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    UINavigationController *destViewController = (UINavigationController*)segue.destinationViewController;
    destViewController.title = [[_menuItems objectAtIndex:indexPath.row-1] capitalizedString];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _menuItems.count+1;
}

//cell hight

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 180;
    }
    else {
        return 61;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //Profile detail Cell
    if (indexPath.row==0) {
        ProfileMenuCell * cell=[tableView dequeueReusableCellWithIdentifier:@"ProfileDetail"];
//        NSManagedObject* profile=[self getProfileName];
//        cell.fullName.text=[profile valueForKey:@"fullName"];
//        NSData * imageData=[profile valueForKey:@"profileImage"];
//        cell.imageProfile.image=[UIImage imageWithData:imageData];
//        cell.imageProfile.layer.borderWidth=2.0;
//        cell.imageProfile.layer.borderColor=[[UIColor whiteColor]CGColor];
//        [cell.imageProfile.layer setCornerRadius:8];
//        cell.imageProfile.clipsToBounds = YES;
//        
        return cell;
    }else{
    
    NSString *CellIdentifier = [_menuItems objectAtIndex:indexPath.row-1];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        return cell;
    }
    
    

}







@end
