//
//  LoginViewController.m
//  GriffithDesign
//
//  Created by Abbas H Safaie on 22/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import "LoginViewController.h"
#import "ApiCustomeCaller.h"
#import "AccessToken.h"
#import <WindowsAzureMobileServices/WindowsAzureMobileServices.h>
#import "Lockbox.h"
#import "SWRevealViewController.h"
@interface LoginViewController ()


@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*Login button hockedUp to view to pass email and password */

- (IBAction)loginButtom:(id)sender {
    
    NSString * userEmail= self.emailTextField.text;
    NSString * pass=self.passwordTextField.text;
    
    
    
    
    if (userEmail.length!=0 && pass.length!=0) {
        NSDictionary * bodyPassed=@{@"email": userEmail, @"password": pass};
        ApiCustomeCaller * apiCall=[[ApiCustomeCaller alloc]init];
        apiCall.body=bodyPassed;
        [apiCall loginHandler]; //Call handler
        
        
        //listiner
        [[NSNotificationCenter defaultCenter]addObserver:self
                                                selector:@selector(recivednotifica:)
                                                    name:LOGIN_RESPONSECODE object:nil];
        
    }else{
        
        //alart user for emplty Fields
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Empty Fields"
                                                        message:@"please enter your Email and Password"
                                                       delegate:nil
                                              cancelButtonTitle:@"Dissmis"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    
}

-(void)recivednotifica:(NSNotification *)notification {
    NSHTTPURLResponse * respondObject= (NSHTTPURLResponse*)[notification userInfo];
    
    
    long status=[respondObject statusCode];
    if (status!=200) {
        NSString * message=[NSString stringWithFormat:@"server respond with %ld",status];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"login Error"
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"Dissmis"
                                              otherButtonTitles:nil];
        [alert show];
    }else{
    
  //  [self performSegueWithIdentifier:@"SlideMenu" sender:self];
        
    }
  
    
}

- (IBAction)registerButton:(id)sender {
}






 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     
     
     
     
     }
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     
     
     
     



@end
