//
//  ImageDto.h
//  GriffithThreaserTry3
//
//  Created by Abbas H Safaie on 10/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiCustomeCaller.h"
#import "AccessToken.h"
#import <WindowsAzureMobileServices/WindowsAzureMobileServices.h>


@interface ImageDto : NSObject<NSURLConnectionDataDelegate>

@property NSString * dateUto;
@property NSString * sasUrl;
@property NSString * url;



@property MSClient * client;
@property NSMutableData * recivedData;
@property NSURLConnection * conn;



-(void) storeImageToBlob:(NSData*)imageData :(NSString*)sasUrl :(NSString*)dateUto;
-(void) saveImageToBlob:(NSData*)image;

@end
