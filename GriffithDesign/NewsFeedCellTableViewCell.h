//
//  NewsFeedCellTableViewCell.h
//  GriffithDesign
//
//  Created by Abbas H Safaie on 22/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsFeedCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *feedTextField;
@property (weak, nonatomic) IBOutlet UILabel *feedName;
@property (weak, nonatomic) IBOutlet UILabel *timeStamp;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;

@end
