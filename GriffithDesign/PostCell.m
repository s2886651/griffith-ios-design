//
//  PostCell.m
//  GriffithDesign
//
//  Created by Abbas H Safaie on 22/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import "PostCell.h"
#import "ApiCustomeCaller.h"

@implementation PostCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)postButton:(id)sender {
    
    ApiCustomeCaller * api = [[ApiCustomeCaller alloc]init];
    
    api.method=@"POST";
    api.apiName=@"Posts";
    NSDictionary * body=@{@"title":@"Test",@"description":self.postText.text};
    api.body=body;
    [api apiCallHandler:POSTS_UPDATE];

}

@end
