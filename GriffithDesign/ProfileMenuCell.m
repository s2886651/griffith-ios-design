//
//  ProfileMenuCell.m
//  GriffithDesign
//
//  Created by Abbas H Safaie on 22/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import "ProfileMenuCell.h"

@implementation ProfileMenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
