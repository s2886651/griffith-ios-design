//
//  PostsTableViewController.h
//  GriffithThreaserTry3
//
//  Created by Abbas H Safaie on 15/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostsTableViewController : UITableViewController

@property NSMutableArray * myData;


@end
