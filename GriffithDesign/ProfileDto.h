//
//  ProfileDto.h
//  GriffithThreaserTry3
//
//  Created by Abbas H Safaie on 7/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import <Foundation/Foundation.h>

#define Profile_firstName  @"firstName"
#define Profile_lastName   @"lastName"
#define Profile_middleName @"middleName"
#define Profile_sex @"sex"
#define Profile_major @"major"
#define Profile_birthDay @"birthDay"
#define Profile_entryYear @"entryYear"



@interface ProfileDto : NSObject

@property NSString * Id;
@property NSString * firstName;
@property NSString * lastName;
@property NSString * middleName;
@property NSString * major;
@property NSDate * entryYear;
@property NSDate * birthDay;


@end
