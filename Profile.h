//
//  Profile.h
//  
//
//  Created by Abbas H Safaie on 27/07/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Profile : NSManagedObject

@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * middleName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * major;
@property (nonatomic, retain) NSString * sex;
@property (nonatomic, retain) NSString * campus;
@property (nonatomic, retain) NSDate * entryYear;
@property (nonatomic, retain) NSDate * birthDay;
@property (nonatomic, retain) NSData * image;

@end
