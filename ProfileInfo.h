//
//  ProfileInfo.h
//  
//
//  Created by Abbas H Safaie on 27/07/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ProfileInfo : NSManagedObject

@property (nonatomic, retain) NSString * fullName;
@property (nonatomic, retain) NSData * profileImage;

@end
