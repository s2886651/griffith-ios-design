//
//  Pro.h
//  GriffithDesign
//
//  Created by Abbas H Safaie on 27/07/2015.
//  Copyright (c) 2015 Abbas H Safaie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pro : NSObject

@property NSString* name;
@property NSString * family;
@end
